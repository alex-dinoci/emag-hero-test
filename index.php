<?php

use Emagia\Battle\BattleFactory;
use Emagia\Battle\Result\DrawResult;
use Emagia\Battle\Result\WinnerResult;
use Emagia\Entity\EntityFactory;
use Emagia\Logger\LoggerFactory;

require_once 'vendor/autoload.php';

$orderus = EntityFactory::buildOrderus();
$wildBeast = EntityFactory::buildWildBeast();

$logger = LoggerFactory::logger();
$battle = BattleFactory::battle($orderus, $wildBeast);

$result = $battle->fight();

$logger->log(PHP_EOL . '-------------------------');

if ($result instanceof DrawResult) {
    $logger->log(PHP_EOL . sprintf('After %d turns, it is a draw.', $battle->getTurn()));
}

if ($result instanceof WinnerResult) {
    $logger->log(PHP_EOL . sprintf('The winner is %s', $result->getWinner()->getName()));
    $logger->logEntityStats($result->getWinner());
}
