# How to install

```
git clone git@gitlab.com:alex-dinoci/emag-hero-test.git emagia
cd emagia
composer install
```

# How to run

```
php index.php
```

# How to run tests

```
vendor/bin/phpspec run
```
