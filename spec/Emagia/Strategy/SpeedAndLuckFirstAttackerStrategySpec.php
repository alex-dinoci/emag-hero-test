<?php

namespace spec\Emagia\Strategy;

use Emagia\Entity\GenericEntity;
use Emagia\Entity\Stats;
use Emagia\Strategy\SpeedAndLuckFirstAttackerStrategy;
use Emagia\ValueObject\Health;
use Emagia\ValueObject\Percentage;
use Emagia\ValueObject\StatsValue;
use PhpSpec\ObjectBehavior;

class SpeedAndLuckFirstAttackerStrategySpec extends ObjectBehavior
{
    /**
     * @var GenericEntity
     */
    private $opponentA;

    /**
     * @var GenericEntity
     */
    private $opponentB;

    function let()
    {
        $this->opponentA = $this->randomOpponent();
        $this->opponentB = $this->randomOpponent();
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(SpeedAndLuckFirstAttackerStrategy::class);
    }

    function it_returns_the_opponent_with_highest_speed()
    {
        $this->opponentA->setSpeed(StatsValue::from(10));
        $this->opponentB->setSpeed(StatsValue::from(20));

        $this->getAttacker($this->opponentA, $this->opponentB)
            ->shouldBe($this->opponentB);

        $this->opponentA->setSpeed(StatsValue::from(40));
        $this->opponentB->setSpeed(StatsValue::from(20));

        $this->getAttacker($this->opponentA, $this->opponentB)
            ->shouldBe($this->opponentA);
    }

    function it_returns_the_opponent_with_highest_luck_when_both_opponents_have_the_same_speed()
    {
        $speed = StatsValue::from(20);
        $this->opponentA->setSpeed($speed);
        $this->opponentB->setSpeed($speed);

        $this->opponentA->setLuck(Percentage::from(50));
        $this->opponentB->setLuck(Percentage::from(30));

        $this->getAttacker($this->opponentA, $this->opponentB)
            ->shouldBe($this->opponentA);

        $this->opponentA->setLuck(Percentage::from(20));
        $this->opponentB->setLuck(Percentage::from(40));

        $this->getAttacker($this->opponentA, $this->opponentB)
            ->shouldBe($this->opponentB);
    }

    private function randomOpponent()
    {
        return new GenericEntity(new Stats(
            Health::from(0), StatsValue::from(0), StatsValue::from(0), StatsValue::from(0), Percentage::from(0)
        ));
    }
}
