<?php

namespace spec\Emagia\Entity;

use Emagia\Entity\Stats;
use Emagia\Skill\AttackSkill;
use Emagia\Skill\DefenceSkill;
use Emagia\Skill\MagicShield;
use Emagia\Skill\RapidStrike;
use Emagia\ValueObject\Health;
use Emagia\ValueObject\Percentage;
use Emagia\ValueObject\StatsValue;
use PhpSpec\ObjectBehavior;

class OrderusSpec extends ObjectBehavior
{
    /**
     * @var Stats
     */
    private $stats;

    function let()
    {
        $this->stats = new Stats(Health::from(75), StatsValue::from(75), StatsValue::from(50), StatsValue::from(45), Percentage::from(20));
        $this->beConstructedWith($this->stats);
    }

    function it_has_health_between_70_and_100()
    {
        $exception = new \InvalidArgumentException('Orderus must have health between 70 and 100.');

        $this->stats->setHealth(Health::from(70));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setHealth(Health::from(100));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setHealth(Health::from(90));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setHealth(Health::from(10));
        $this->shouldThrow($exception)->duringInstantiation();
    }

    function it_has_strength_between_70_and_80()
    {
        $exception = new \InvalidArgumentException('Orderus must have strength between 70 and 80.');

        $this->stats->setStrength(StatsValue::from(70));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setStrength(StatsValue::from(80));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setStrength(StatsValue::from(75));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setStrength(StatsValue::from(10));
        $this->shouldThrow($exception)->duringInstantiation();
    }

    function it_has_defence_between_45_and_55()
    {
        $exception = new \InvalidArgumentException('Orderus must have defence between 45 and 55.');

        $this->stats->setDefence(StatsValue::from(45));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setDefence(StatsValue::from(55));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setDefence(StatsValue::from(50));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setDefence(StatsValue::from(10));
        $this->shouldThrow($exception)->duringInstantiation();
    }

    function it_has_speed_between_40_and_50()
    {
        $exception = new \InvalidArgumentException('Orderus must have speed between 40 and 50.');

        $this->stats->setSpeed(StatsValue::from(40));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setSpeed(StatsValue::from(50));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setSpeed(StatsValue::from(45));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setSpeed(StatsValue::from(10));
        $this->shouldThrow($exception)->duringInstantiation();
    }

    function it_has_luck_between_10_and_30()
    {
        $exception = new \InvalidArgumentException('Orderus must have luck between 10% and 30%.');

        $this->stats->setLuck(Percentage::from(10));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setLuck(Percentage::from(30));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setLuck(Percentage::from(20));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setLuck(Percentage::from(50));
        $this->shouldThrow($exception)->duringInstantiation();
    }

    function it_possesses_rapid_strike_skill()
    {
        $this->getSkills(AttackSkill::class)[0]->shouldBeAnInstanceOf(RapidStrike::class);
    }

    function it_possesses_magic_shield_skill()
    {
        $this->getSkills(DefenceSkill::class)[0]->shouldBeAnInstanceOf(MagicShield::class);
    }
}
