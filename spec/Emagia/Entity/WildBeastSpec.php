<?php

namespace spec\Emagia\Entity;

use Emagia\Entity\Stats;
use Emagia\ValueObject\Health;
use Emagia\ValueObject\Percentage;
use Emagia\ValueObject\StatsValue;
use PhpSpec\ObjectBehavior;

class WildBeastSpec extends ObjectBehavior
{
    /**
     * @var Stats
     */
    private $stats;

    function let()
    {
        $this->stats = new Stats(Health::from(80), StatsValue::from(80), StatsValue::from(50), StatsValue::from(50), Percentage::from(30));
        $this->beConstructedWith($this->stats);
    }

    function it_has_health_between_60_and_90()
    {
        $exception = new \InvalidArgumentException('Wild beast must have health between 60 and 90.');

        $this->stats->setHealth(Health::from(60));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setHealth(Health::from(90));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setHealth(Health::from(80));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setHealth(Health::from(10));
        $this->shouldThrow($exception)->duringInstantiation();
    }

    function it_has_strength_between_60_and_90()
    {
        $exception = new \InvalidArgumentException('Wild beast must have strength between 60 and 90.');

        $this->stats->setStrength(StatsValue::from(60));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setStrength(StatsValue::from(90));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setStrength(StatsValue::from(75));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setStrength(StatsValue::from(10));
        $this->shouldThrow($exception)->duringInstantiation();
    }

    function it_has_defence_between_40_and_60()
    {
        $exception = new \InvalidArgumentException('Wild beast must have defence between 40 and 60.');

        $this->stats->setDefence(StatsValue::from(40));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setDefence(StatsValue::from(60));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setDefence(StatsValue::from(50));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setDefence(StatsValue::from(10));
        $this->shouldThrow($exception)->duringInstantiation();
    }

    function it_has_speed_between_40_and_60()
    {
        $exception = new \InvalidArgumentException('Wild beast must have speed between 40 and 60.');

        $this->stats->setSpeed(StatsValue::from(40));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setSpeed(StatsValue::from(60));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setSpeed(StatsValue::from(50));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setSpeed(StatsValue::from(10));
        $this->shouldThrow($exception)->duringInstantiation();
    }

    function it_has_luck_between_25_and_40()
    {
        $exception = new \InvalidArgumentException('Wild beast must have luck between 25% and 40%.');

        $this->stats->setLuck(Percentage::from(25));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setLuck(Percentage::from(40));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setLuck(Percentage::from(30));
        $this->shouldNotThrow($exception)->duringInstantiation();

        $this->stats->setLuck(Percentage::from(50));
        $this->shouldThrow($exception)->duringInstantiation();
    }
}
