<?php

namespace spec\Emagia\Entity;

use Emagia\Battle\Strike;
use Emagia\Entity\GenericEntity;
use Emagia\Entity\Stats;
use Emagia\Skill\MagicShield;
use Emagia\Skill\RapidStrike;
use Emagia\ValueObject\Health;
use Emagia\ValueObject\Percentage;
use Emagia\ValueObject\StatsValue;
use PhpSpec\ObjectBehavior;

class GenericEntitySpec extends ObjectBehavior
{
    function let()
    {
        $stats = new Stats(Health::from(10), StatsValue::from(50), StatsValue::from(10), StatsValue::from(10), new Percentage(10));

        $this->beConstructedWith($stats);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(GenericEntity::class);
    }

    function it_is_alive_when_health_is_above_zero()
    {
        $this->isAlive()->shouldReturn(true);

        $this->receiveDamage(100);

        $this->isAlive()->shouldReturn(false);
    }

    function it_attacks_a_target(GenericEntity $target)
    {
        $target->dodgeNextAttack()->willReturn(false);

        $target->defend(new Strike(50))->shouldBeCalled();

        $this->attack($target);
    }

    function it_misses_an_attack_if_target_is_lucky_and_dodges(GenericEntity $target)
    {
        $target->dodgeNextAttack()->willReturn(true);

        $target->defend(new Strike(50))->shouldNotBeCalled();

        $this->attack($target);
    }

    function it_empowers_attack_using_skills(GenericEntity $target, RapidStrike $rapidStrike)
    {
        $this->addSkill($rapidStrike);

        $target->dodgeNextAttack()->willReturn(false);
        $target->defend(new Strike(50))->shouldBeCalled();

        $rapidStrike->wasUsed()->shouldBeCalled();
        $rapidStrike->empowerAttack($this, $target)->shouldBeCalled();

        $this->attack($target);
    }

    function it_defends_from_a_strike_taking_damage_as_strength_minus_defence()
    {
        $this->beConstructedWith(new Stats(
            Health::from(50), StatsValue::from(50), StatsValue::from(10), StatsValue::from(10), new Percentage(10))
        );

        $strike = new Strike(20);

        $this->getHealth()->getValue()->shouldReturn(50.0);
        $this->getDefence()->shouldReturn(10);

        $this->defend($strike);
        $this->getHealth()->getValue()->shouldReturn(40.0);
    }

    function it_takes_no_damage_if_defence_is_higher_than_strength()
    {
        $this->beConstructedWith(new Stats(Health::from(50), StatsValue::from(50), StatsValue::from(50), StatsValue::from(10), new Percentage(10)));

        $strike = new Strike(20);

        $this->getHealth()->getValue()->shouldReturn(50.0);
        $this->defend($strike);
        $this->getHealth()->getValue()->shouldReturn(50.0);
    }

    function it_mitigates_damage_when_defending_using_skills(MagicShield $magicShield)
    {
        $initialHealth = 10.0;
        $remainingHealthWithoutMitigation = 5.0;
        $remainingHealthWithMitigation = 7.5;

        $this->addSkill($magicShield);

        $strike = new Strike(15);

        $this->getHealth()->getValue()->shouldReturn($initialHealth);

        $magicShield->wasUsed()->shouldBeCalled();
        $magicShield->mitigateDamage($this, 5)->willReturn(2.5);

        $this->defend($strike);

        $this->getHealth()->getValue()->shouldReturn($remainingHealthWithMitigation);
        $this->getHealth()->getValue()->shouldNotReturn($remainingHealthWithoutMitigation);
    }

    function it_receives_damage()
    {
        $this->getHealth()->getValue()->shouldReturn(10.0);

        $this->receiveDamage(8.0);

        $this->getHealth()->getValue()->shouldReturn(2.0);
    }
}
