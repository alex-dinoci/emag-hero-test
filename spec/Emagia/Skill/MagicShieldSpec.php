<?php

namespace spec\Emagia\Skill;

use Emagia\Entity\GenericEntity;
use Emagia\Skill\MagicShield;
use Emagia\ValueObject\Percentage;
use PhpSpec\ObjectBehavior;

class MagicShieldSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(MagicShield::class);
    }

    function it_has_20_percent_chance_to_be_used()
    {
        $this->getChanceToUseIt()->getValue()->shouldReturn(20);
    }

    function it_is_not_used_if_unlucky(GenericEntity $defender)
    {
        $this->beConstructedWith(new Percentage(0));

        $this->mitigateDamage($defender, 10.0)->shouldReturn(10.0);
        $this->mitigateDamage($defender, 10.0)->shouldReturn(10.0);
        $this->mitigateDamage($defender, 10.0)->shouldReturn(10.0);
        $this->mitigateDamage($defender, 10.0)->shouldReturn(10.0);
    }

    function it_mitigates_half_damage_when_used(GenericEntity $defender)
    {
        $this->beConstructedWith(new Percentage(100));

        $this->mitigateDamage($defender, 10.0)->shouldReturn(5.0);
        $this->mitigateDamage($defender, 15.0)->shouldReturn(7.5);
    }
}
