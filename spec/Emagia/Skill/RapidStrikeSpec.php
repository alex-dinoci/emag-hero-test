<?php

namespace spec\Emagia\Skill;

use Emagia\Entity\GenericEntity;
use Emagia\Skill\RapidStrike;
use Emagia\ValueObject\Percentage;
use PhpSpec\ObjectBehavior;

class RapidStrikeSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(RapidStrike::class);
    }

    function it_has_10_percent_chance_to_be_used()
    {
        $this->getChanceToUseIt()->getValue()->shouldReturn(10);
    }

    function it_is_not_used_if_unlucky(GenericEntity $attacker, GenericEntity $defender)
    {
        $this->beConstructedWith(new Percentage(0));

        $attacker->strike($defender)->shouldNotBeCalled();

        $this->empowerAttack($attacker, $defender);
        $this->empowerAttack($attacker, $defender);
        $this->empowerAttack($attacker, $defender);
        $this->empowerAttack($attacker, $defender);
        $this->empowerAttack($attacker, $defender);
    }

    function it_strikes_again_when_used(GenericEntity $attacker, GenericEntity $defender)
    {
        $this->beConstructedWith(new Percentage(100));
        $attacker->getName()->willReturn('The Attacker');

        $attacker->strike($defender)->shouldBeCalled();

        $this->empowerAttack($attacker, $defender);
    }
}
