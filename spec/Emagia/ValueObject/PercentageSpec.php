<?php

namespace spec\Emagia\ValueObject;

use Emagia\ValueObject\Percentage;
use PhpSpec\ObjectBehavior;

class PercentageSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Percentage::class);
    }

    function it_must_be_greater_than_0()
    {
        $exception = new \InvalidArgumentException('Percentage must be greater than 0.');

        $this->beConstructedWith(-10);
        $this->shouldThrow($exception)->duringInstantiation();

        $this->beConstructedWith(0);
        $this->shouldNotThrow($exception)->duringInstantiation();
    }

    function it_must_be_less_than_100()
    {
        $exception = new \InvalidArgumentException('Percentage must be less than 100.');

        $this->beConstructedWith(110);
        $this->shouldThrow($exception)->duringInstantiation();

        $this->beConstructedWith(100);
        $this->shouldNotThrow($exception)->duringInstantiation();
    }
}
