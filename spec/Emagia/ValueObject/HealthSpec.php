<?php

namespace spec\Emagia\ValueObject;

use Emagia\ValueObject\Health;
use PhpSpec\ObjectBehavior;

class HealthSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith(50);
        $this->shouldHaveType(Health::class);
    }

    function it_cannot_be_less_than_0()
    {
        $this->beConstructedWith(-10);
        $this->getValue()->shouldReturn(0.0);
    }
}
