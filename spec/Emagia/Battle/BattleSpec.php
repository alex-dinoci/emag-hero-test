<?php

namespace spec\Emagia\Battle;

use Emagia\Battle\Battle;
use Emagia\Battle\Result\DrawResult;
use Emagia\Battle\Result\WinnerResult;
use Emagia\Entity\GenericEntity;
use Emagia\Strategy\FirstAttackerStrategy;
use PhpSpec\ObjectBehavior;

class BattleSpec extends ObjectBehavior
{
    private $maxTurns = 20;
    private $opponentA;
    private $opponentB;

    function let(FirstAttackerStrategy $firstAttackerStrategy, GenericEntity $opponentA, GenericEntity $opponentB)
    {
        $this->opponentA = $opponentA;
        $this->opponentB = $opponentB;

        $firstAttackerStrategy->getAttacker($this->opponentA, $this->opponentB)
            ->willReturn($this->opponentA);

        $this->beConstructedWith($this->opponentA, $this->opponentB,$firstAttackerStrategy, $this->maxTurns);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Battle::class);
    }

    function it_ends_in_a_draw_if_it_reaches_the_maximum_number_of_turns()
    {
        $this->opponentA->attack($this->opponentB)
            ->shouldBeCalledTimes(10);
        $this->opponentB->attack($this->opponentA)
            ->shouldBeCalledTimes(10);

        $this->opponentA->isDead()->willReturn(false);
        $this->opponentB->isDead()->willReturn(false);

        $this->fight()
            ->shouldBeAnInstanceOf(DrawResult::class);

        $this->getTurn()->shouldReturn($this->maxTurns);
    }

    function it_ends_with_a_winner_when_one_opponent_dies()
    {
        $this->opponentA->attack($this->opponentB)->shouldBeCalled();
        $this->opponentB->isDead()->willReturn(true);

        $result = $this->fight();

        $result->shouldBeAnInstanceOf(WinnerResult::class);
        $result->getWinner()->shouldReturn($this->opponentA);
    }

    function it_swaps_attacker_and_defender_after_each_turn()
    {
        $this->getAttacker()->shouldReturn($this->opponentA);
        $this->getDefender()->shouldReturn($this->opponentB);

        $this->nextTurn();

        $this->getAttacker()->shouldReturn($this->opponentB);
        $this->getDefender()->shouldReturn($this->opponentA);

        $this->nextTurn();

        $this->getAttacker()->shouldReturn($this->opponentA);
        $this->getDefender()->shouldReturn($this->opponentB);
    }
}
