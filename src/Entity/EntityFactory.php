<?php

namespace Emagia\Entity;

use Emagia\Logger\LoggerFactory;
use Emagia\ValueObject\Health;
use Emagia\ValueObject\Percentage;
use Emagia\ValueObject\StatsValue;

abstract class EntityFactory
{
    public static function buildOrderus()
    {
        $orderus = new Orderus(new Stats(
            Health::randomBetween(70, 100),
            StatsValue::randomBetween(70, 80),
            StatsValue::randomBetween(45, 55),
            StatsValue::randomBetween(40, 50),
            Percentage::randomBetween(10, 30)
        ));

        $orderus->setLogger(LoggerFactory::logger());

        return $orderus;
    }

    public static function buildWildBeast()
    {
        $wildBeast = new WildBeast(new Stats(
            Health::randomBetween(60, 90),
            StatsValue::randomBetween(60, 90),
            StatsValue::randomBetween(40, 60),
            StatsValue::randomBetween(40, 60),
            Percentage::randomBetween(25, 40)
        ));

        $wildBeast->setLogger(LoggerFactory::logger());

        return $wildBeast;
    }
}
