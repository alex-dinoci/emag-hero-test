<?php

namespace Emagia\Entity;

use Emagia\ValueObject\Health;
use Emagia\ValueObject\Percentage;
use Emagia\ValueObject\StatsValue;

class Stats
{
    /**
     * @var Health
     */
    private $health;

    /**
     * @var StatsValue
     */
    private $strength;

    /**
     * @var StatsValue
     */
    private $defence;

    /**
     * @var StatsValue
     */
    private $speed;

    /**
     * @var Percentage
     */
    private $luck;

    public function __construct(Health $health,
                                StatsValue $strength,
                                StatsValue $defence,
                                StatsValue $speed,
                                Percentage $luck)
    {
        $this->health = $health;
        $this->strength = $strength;
        $this->defence = $defence;
        $this->speed = $speed;
        $this->luck = $luck;
    }

    public function getHealth(): Health
    {
        return $this->health;
    }

    public function setHealth(Health $health)
    {
        $this->health = $health;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength->getValue();
    }

    /**
     * @param StatsValue $strength
     */
    public function setStrength(StatsValue $strength)
    {
        $this->strength = $strength;
    }

    /**
     * @return int
     */
    public function getDefence(): int
    {
        return $this->defence->getValue();
    }

    /**
     * @param StatsValue $defence
     */
    public function setDefence(StatsValue $defence)
    {
        $this->defence = $defence;
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed->getValue();
    }

    /**
     * @param StatsValue $speed
     */
    public function setSpeed(StatsValue $speed)
    {
        $this->speed = $speed;
    }

    /**
     * @return int
     */
    public function getLuck(): int
    {
        return $this->luck->getValue();
    }

    /**
     * @param Percentage $luck
     */
    public function setLuck(Percentage $luck)
    {
        $this->luck = $luck;
    }
}
