<?php

namespace Emagia\Entity;

use Emagia\Skill\MagicShield;
use Emagia\Skill\RapidStrike;

class Orderus extends GenericEntity
{
    protected $name = 'Orderus';

    public function __construct(Stats $stats)
    {
        if ($stats->getHealth()->getValue() < 70 || $stats->getHealth()->getValue() > 100) {
            throw new \InvalidArgumentException('Orderus must have health between 70 and 100.');
        }

        if ($stats->getStrength() < 70 || $stats->getStrength() > 80) {
            throw new \InvalidArgumentException('Orderus must have strength between 70 and 80.');
        }

        if ($stats->getDefence() < 45 || $stats->getDefence() > 55) {
            throw new \InvalidArgumentException('Orderus must have defence between 45 and 55.');
        }

        if ($stats->getSpeed() < 40 || $stats->getSpeed() > 50) {
            throw new \InvalidArgumentException('Orderus must have speed between 40 and 50.');
        }

        if ($stats->getLuck() < 10 || $stats->getLuck() > 30) {
            throw new \InvalidArgumentException('Orderus must have luck between 10% and 30%.');
        }

        parent::__construct($stats);

        $this->addSkill(new RapidStrike());
        $this->addSkill(new MagicShield());
    }
}
