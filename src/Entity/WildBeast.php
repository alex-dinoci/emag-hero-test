<?php

namespace Emagia\Entity;

class WildBeast extends GenericEntity
{
    protected $name = 'Wild Beast';

    public function __construct(Stats $stats)
    {
        if ($stats->getHealth()->getValue() < 60 || $stats->getHealth()->getValue() > 90) {
            throw new \InvalidArgumentException('Wild beast must have health between 60 and 90.');
        }

        if ($stats->getStrength() < 60 || $stats->getStrength() > 90) {
            throw new \InvalidArgumentException('Wild beast must have strength between 60 and 90.');
        }

        if ($stats->getDefence() < 40 || $stats->getDefence() > 60) {
            throw new \InvalidArgumentException('Wild beast must have defence between 40 and 60.');
        }

        if ($stats->getSpeed() < 40 || $stats->getSpeed() > 60) {
            throw new \InvalidArgumentException('Wild beast must have speed between 40 and 60.');
        }

        if ($stats->getLuck() < 25 || $stats->getLuck() > 40) {
            throw new \InvalidArgumentException('Wild beast must have luck between 25% and 40%.');
        }

        parent::__construct($stats);
    }
}
