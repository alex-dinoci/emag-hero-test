<?php

namespace Emagia\Entity;

use Emagia\Battle\Strike;
use Emagia\Logger\Logger;
use Emagia\Skill\AttackSkill;
use Emagia\Skill\DefenceSkill;
use Emagia\Skill\Skill;
use Emagia\ValueObject\Health;
use Emagia\ValueObject\Percentage;
use Emagia\ValueObject\StatsValue;

class GenericEntity
{
    /**
     * @var Stats
     */
    protected $stats;

    protected $name;

    protected $skills = [];

    /**
     * @var Logger
     */
    protected $logger;

    public function __construct(Stats $stats)
    {
        $this->stats = $stats;
    }

    public function isAlive()
    {
        return !$this->isDead();
    }

    public function isDead()
    {
        return $this->stats->getHealth()->isZero();
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function addSkill(Skill $skill)
    {
        $this->skills[] = $skill;
    }

    public function getSkills(string $type)
    {
        return array_values(array_filter($this->skills, function ($skill) use ($type) {
            return $skill instanceof $type;
        }));
    }

    /**
     * @return Skill[]
     */
    public function getAllSkills()
    {
        return $this->skills;
    }

    public function getSpeed()
    {
        return $this->stats->getSpeed();
    }

    public function setSpeed(StatsValue $speed)
    {
        $this->stats->setSpeed($speed);
    }

    public function getLuck()
    {
        return $this->stats->getLuck();
    }

    public function setLuck(Percentage $luck)
    {
        $this->stats->setLuck($luck);
    }

    public function getStrength()
    {
        return $this->stats->getStrength();
    }

    public function getDefence()
    {
        return $this->stats->getDefence();
    }

    public function getHealth()
    {
        return $this->stats->getHealth();
    }

    public function getName()
    {
        return $this->name;
    }

    public function dodgeNextAttack()
    {
        return random_int(1, 100) <= $this->getLuck();
    }

    public function attack(GenericEntity $target)
    {
        null !== $this->logger && $this->logger->log(sprintf(
            '%s is attacking %s', $this->getName(), $target->getName()
        ));

        if ($target->dodgeNextAttack()) {
            null !== $this->logger && $this->logger->log(sprintf(
                '%s is lucky and dodges next attack', $target->getName()
            ));
            return;
        }

        /** @var AttackSkill|Skill $skill */
        foreach ($this->getSkills(AttackSkill::class) as $skill) {
            $skill->empowerAttack($this, $target);

            if (!$skill->wasUsed()) {
                continue;
            }

            null !== $this->logger && $this->logger->log($skill->getMessage());
        }

        $this->strike($target);
    }

    public function strike(GenericEntity $target)
    {
        $strike = new Strike($this->getStrength());

        null !== $this->logger && $this->logger->log(sprintf(
            '%s strikes %s with strength %d', $this->getName(), $target->getName(), $this->getStrength()
        ));

        $target->defend($strike);
    }

    public function receiveDamage(float $damage)
    {
        $health = Health::from($this->getHealth()->getValue() - $damage);
        $this->stats->setHealth($health);

        null !== $this->logger && $this->logger->log(sprintf(
            '%s receives %.2f damage and has %.2f health left', $this->getName(), $damage, $this->getHealth()->getValue()
        ));
    }

    public function defend(Strike $strike)
    {
        null !== $this->logger && $this->logger->log(sprintf(
            '%s defends strike with defence %d and health %.2f', $this->getName(), $this->getDefence(), $this->getHealth()->getValue()
        ));

        $damage = $strike->getStrength() - $this->getDefence();

        if ($damage <= 0) {
            null !== $this->logger && $this->logger->log(sprintf(
                '%s takes no damage', $this->getName()
            ));

            return;
        }

        /** @var DefenceSkill|Skill $skill */
        foreach ($this->getSkills(DefenceSkill::class) as $skill) {
            $damage = $skill->mitigateDamage($this, $damage);

            if (!$skill->wasUsed()) {
                continue;
            }

            null !== $this->logger && $this->logger->log($skill->getMessage());
        }

        $this->receiveDamage($damage);
    }
}

