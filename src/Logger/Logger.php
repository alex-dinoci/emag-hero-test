<?php

namespace Emagia\Logger;

use Emagia\Entity\GenericEntity;

interface Logger
{
    public function log(string $message);

    public function logEntityStats(GenericEntity $entity);
}
