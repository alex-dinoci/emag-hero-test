<?php

namespace Emagia\Logger;

abstract class LoggerFactory
{
    public static function logger()
    {
        static $logger = null;

        if (null === $logger) {
            $logger = new CliLogger();
        }

        return $logger;
    }
}
