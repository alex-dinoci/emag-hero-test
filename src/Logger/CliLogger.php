<?php

namespace Emagia\Logger;

use Emagia\Entity\GenericEntity;

class CliLogger implements Logger
{
    public function log(string $message)
    {
        echo $message . PHP_EOL;
    }

    public function logEntityStats(GenericEntity $entity)
    {
        $this->log(sprintf(
            '%s has %.2f health, %d strength, %d defence, %d speed, %d%% luck',
            $entity->getName(),
            $entity->getHealth()->getValue(),
            $entity->getStrength(),
            $entity->getDefence(),
            $entity->getSpeed(),
            $entity->getLuck()
        ));
    }
}
