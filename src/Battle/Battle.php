<?php

namespace Emagia\Battle;

use Emagia\Battle\Result\DrawResult;
use Emagia\Battle\Result\Result;
use Emagia\Battle\Result\WinnerResult;
use Emagia\Entity\GenericEntity;
use Emagia\Logger\Logger;
use Emagia\Strategy\FirstAttackerStrategy;

class Battle
{
    private $turn = 0;

    /**
     * @var GenericEntity
     */
    private $attacker;

    /**
     * @var GenericEntity
     */
    private $defender;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var int
     */
    private $maxTurns;

    /**
     * @var FirstAttackerStrategy
     */
    private $firstAttackerStrategy;

    public function __construct(GenericEntity $opponentA,
                                GenericEntity $opponentB,
                                FirstAttackerStrategy $firstAttackerStrategy,
                                int $maxTurns,
                                Logger $logger = null)
    {

        $this->logger = $logger;
        $this->maxTurns = $maxTurns;
        $this->firstAttackerStrategy = $firstAttackerStrategy;

        $this->firstAttacker($opponentA, $opponentB);
    }

    public function fight()
    {
        $this->initializeFight();

        while ($this->maxTurnsNotReached()) {
            try {
                $this->nextTurn();
            }
            catch (DefenderDiedException $e) {
                return new WinnerResult($this->attacker);
            }
        }

        return new DrawResult();
    }

    public function nextTurn()
    {
        $this->initializeNextTurn();

        $this->attacker->attack($this->defender);

        if ($this->defenderDied()) {
            null !== $this->logger && $this->logger->log(sprintf(
                '%s has been defeated', $this->defender->getName()
            ));
            throw new DefenderDiedException();
        }

        $this->swapAttackerAndDefender();
    }

    public function getAttacker()
    {
        return $this->attacker;
    }

    public function getDefender()
    {
        return $this->defender;
    }

    public function getTurn()
    {
        return $this->turn;
    }

    private function firstAttacker(GenericEntity $opponentA, GenericEntity $opponentB)
    {
        $firstAttacker = $this->firstAttackerStrategy->getAttacker($opponentA, $opponentB);

        if ($firstAttacker === $opponentA) {
            $this->attacker = $opponentA;
            $this->defender = $opponentB;
        }
        else {
            $this->attacker = $opponentB;
            $this->defender = $opponentA;
        }
    }

    private function initializeFight()
    {
        if (null !== $this->logger) {
            $this->logger->log('The combatants are:');
            $this->logger->logEntityStats($this->attacker);
            $this->logger->logEntityStats($this->defender);

            $this->logger->log('------------------------');
            $this->logger->log(sprintf('First to attack is %s', $this->attacker->getName()));
        }
    }

    private function initializeNextTurn()
    {
        $this->turn++;

        if (null !== $this->logger) {
            $this->logger->log(PHP_EOL . sprintf('TURN NO. %d', $this->turn));
        }
    }

    private function maxTurnsNotReached()
    {
        return $this->turn < $this->maxTurns;
    }

    private function defenderDied()
    {
        return $this->defender->isDead();
    }

    private function swapAttackerAndDefender()
    {
        [$this->attacker, $this->defender] = [$this->defender, $this->attacker];
    }
}
