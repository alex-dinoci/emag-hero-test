<?php

namespace Emagia\Battle\Result;

use Emagia\Entity\GenericEntity;

class WinnerResult implements Result
{
    /**
     * @var GenericEntity
     */
    private $winner;

    public function __construct(GenericEntity $winner)
    {
        $this->winner = $winner;
    }

    /**
     * @return GenericEntity
     */
    public function getWinner(): GenericEntity
    {
        return $this->winner;
    }
}
