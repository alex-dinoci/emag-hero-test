<?php

namespace Emagia\Battle;

class Strike
{
    /**
     * @var int
     */
    private $strength;

    public function __construct(int $strength)
    {
        $this->strength = $strength;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }
}
