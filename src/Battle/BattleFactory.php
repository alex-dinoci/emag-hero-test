<?php

namespace Emagia\Battle;

use Emagia\Entity\GenericEntity;
use Emagia\Logger\LoggerFactory;
use Emagia\Strategy\SpeedAndLuckFirstAttackerStrategy;

abstract class BattleFactory
{
    public static function battle(GenericEntity $opponentA, GenericEntity $opponentB)
    {
        $logger = LoggerFactory::logger();

        return new Battle(
            $opponentA,
            $opponentB,
            new SpeedAndLuckFirstAttackerStrategy(),
            20,
            $logger
        );
    }
}
