<?php

namespace Emagia\Strategy;

use Emagia\Entity\GenericEntity;

interface FirstAttackerStrategy
{
    public function getAttacker(GenericEntity $opponentA, GenericEntity $opponentB);
}
