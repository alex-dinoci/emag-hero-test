<?php

namespace Emagia\Strategy;

use Emagia\Entity\GenericEntity;

class SpeedAndLuckFirstAttackerStrategy implements FirstAttackerStrategy
{
    public function getAttacker(GenericEntity $opponentA, GenericEntity $opponentB)
    {
        if ($opponentA->getSpeed() !== $opponentB->getSpeed()) {
            return $opponentA->getSpeed() > $opponentB->getSpeed() ? $opponentA : $opponentB;
        }

        if ($opponentA->getLuck() !== $opponentB->getLuck()) {
            return $opponentA->getLuck() > $opponentB->getLuck() ? $opponentA : $opponentB;
        }

//        throw new \Exception('Universe imploded...');

        return $opponentA;
    }
}
