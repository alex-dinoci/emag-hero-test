<?php

namespace Emagia\ValueObject;

class StatsValue
{
    /**
     * @var int
     */
    private $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public static function from(int $value)
    {
        return new self($value);
    }

    public static function randomBetween(int $min, int $max)
    {
        return new self(random_int($min, $max));
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}
