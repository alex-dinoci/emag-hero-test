<?php

namespace Emagia\ValueObject;

class Health
{
    /**
     * @var float
     */
    private $value;

    public function __construct(float $value)
    {
        if ($value < 0) {
            $value = 0.0;
        }

        $this->value = $value;
    }

    public static function from(float $value)
    {
        return new self($value);
    }

    public static function randomBetween(int $min, int $max)
    {
        return new self((float)random_int($min, $max));
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    public function isZero()
    {
        return $this->value === 0.0;
    }
}
