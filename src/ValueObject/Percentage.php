<?php

namespace Emagia\ValueObject;

class Percentage
{
    /**
     * @var int
     */
    private $value;

    public function __construct(int $value = 0)
    {
        if ($value < 0) {
            throw new \InvalidArgumentException('Percentage must be greater than 0.');
        }

        if ($value > 100) {
            throw new \InvalidArgumentException('Percentage must be less than 100.');
        }

        $this->value = $value;
    }

    public static function from(int $value = 0)
    {
        return new self($value);
    }

    public static function randomBetween(int $min, int $max)
    {
        return new self(random_int($min, $max));
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}
