<?php

namespace Emagia\Skill;

use Emagia\Entity\GenericEntity;

interface DefenceSkill
{
    public function mitigateDamage(GenericEntity $defender, float $damage): float;
}
