<?php

namespace Emagia\Skill;

use Emagia\Entity\GenericEntity;
use Emagia\ValueObject\Percentage;

class MagicShield extends ChanceBasedSkill implements DefenceSkill
{
    private $damageMitigationPercentage;

    public function __construct(Percentage $chanceToUseIt = null)
    {
        $this->setChanceToUseIt($chanceToUseIt ?? new Percentage(20));
        $this->damageMitigationPercentage = new Percentage(50);
    }

    public function mitigateDamage(GenericEntity $defender, float $damage): float
    {
        $this->wasUsed = false;

        if (!$this->isLuckyToUseIt()) {
            return $damage;
        }

        $this->wasUsed = true;
        $this->message = sprintf(
            '%s uses Magic Shield to mitigate half the damage', $defender->getName()
        );

        return $damage * ($this->damageMitigationPercentage->getValue() / 100);
    }
}
