<?php

namespace Emagia\Skill;

use Emagia\Entity\GenericEntity;
use Emagia\ValueObject\Percentage;

class RapidStrike extends ChanceBasedSkill implements AttackSkill
{
    public function __construct(Percentage $chanceToUseIt = null)
    {
        $this->setChanceToUseIt($chanceToUseIt ?? new Percentage(10));
    }

    public function empowerAttack(GenericEntity $attacker, GenericEntity $defender)
    {
        $this->wasUsed = false;

        if (!$this->isLuckyToUseIt()) {
            return;
        }

        $this->wasUsed = true;
        $this->message = sprintf(
            '%s uses Rapid Strike to strike one more time', $attacker->getName()
        );

        $attacker->strike($defender);
    }
}
