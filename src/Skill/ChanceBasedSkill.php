<?php

namespace Emagia\Skill;

use Emagia\ValueObject\Percentage;

abstract class ChanceBasedSkill implements Skill
{
    /**
     * @var Percentage
     */
    protected $chanceToUseIt;

    protected $wasUsed = false;

    protected $message = '';

    /**
     * @return bool
     */
    public function wasUsed(): bool
    {
        return $this->wasUsed;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    public function getChanceToUseIt()
    {
        return $this->chanceToUseIt;
    }

    protected function isLuckyToUseIt()
    {
        return random_int(1, 100) <= $this->chanceToUseIt->getValue();
    }

    protected function setChanceToUseIt(Percentage $percentage)
    {
        $this->chanceToUseIt = $percentage;
    }
}
