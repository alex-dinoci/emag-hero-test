<?php

namespace Emagia\Skill;

use Emagia\Entity\GenericEntity;

interface AttackSkill
{
    public function empowerAttack(GenericEntity $attacker, GenericEntity $defender);
}
