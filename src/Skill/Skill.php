<?php

namespace Emagia\Skill;

interface Skill
{
    public function wasUsed();

    public function getMessage();
}
